import os
import re
import shlex
import subprocess
from pype.env_modules import get_module_cmd, program_string

N_CPU = 10

def requirements():
    return({'ncpu': N_CPU, 'time': '800:00:00', 'mem': '6gb'})

def results(argv):
    sv_type = argv['--sv_type']
    try:
        output = argv['--out']
    except KeyError:
        output = argv['-o']
    output = re.sub('.vcf$', '', output)
    output = '%s_%s' % (re.sub('.bcf$', '', output), sv_type)
    return({'bcf': '%s.bcf' % output, 'idx': '%s.bcf.csi' % output,
            'somatic': '%s.somatic.bcf' % output,
            'somatic_idx': '%s.somatic.bcf.csi' % output,
            'germline': '%s.germline.bcf' % output,
            'germline_idx': '%s.germline.bcf.csi' % output})
            
def add_parser(subparsers, module_name):
    return subparsers.add_parser(module_name,
                                 help=('Structural variant '
                                       'discovery by integrated paired-end '
                                       'and split-read analysis'),
                                 add_help=False)
                                 
def delly_args(parser, subparsers, argv):
    parser.add_argument('-n', '--normal', dest='normal',
                        help='Normal sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-t', '--tumor', dest='tumor',
                        help='Tumor sorted and indexed BAM file',
                        required=True)
    parser.add_argument('-o', '--out', dest='out',
                        help='Output BCF file', required=True)
    parser.add_argument('--sv_type', dest='sv_type', action='store',
                        choices=['DEL', 'DUP', 'INV', 'TRA', 'INS'],
                        help='SV type, default DEL', default='DEL')
    parser.add_argument('--no-exclude', dest='no_exclude',
                        help='Do not exclude regions', action='store_true')
    return parser.parse_args(argv)
    
def delly(parser, subparsers, module_name, argv, profile, log):

    log.log.info('Process snippets arguments')
    args = delly_args(add_parser(
        subparsers, module_name), subparsers, argv)

    log.log.info('Load env module modules')
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['delly']))
    # delly-fitler for the sv_add_rn.py command (depends on bedtools)
    module('add', program_string(profile.programs['bedtools']))
    module('add', program_string(profile.programs['delly-filter']))
    # bcftools to index the final bcf file
    module('add', program_string(profile.programs['bcftools']))

    build = profile.genome_build
    log.log.info('Use genome build %s' % build)

    genome = profile.files['genome_fa']
    log.log.info('Use genome reference %s' % genome)

    if args.no_exclude is False:
        ref_exclude = profile.files['gencode_excl']
        log.log.info('Use exclude regions file %s' % ref_exclude)

    log.log.info('Set environment variable OMP_NUM_THREADS=%s' % N_CPU)
    os.environ['OMP_NUM_THREADS'] = '%s' % N_CPU

    output = re.sub('.gz$', '', args.out)
    output = re.sub('.vcf$', '', args.out)
    output = '%s_%s' % (re.sub('.bcf$', '', output), args.sv_type)
    
    output_dir, output_file = os.path.split(os.path.abspath(output))
    if not os.path.exists(output_dir):
        log.log.info('Create output directory %s' % output_dir)
        os.makedirs(output_dir)
    delly_version = program_version('delly')
    log.log.info('Delly version %s' % '.'.join(map(str, delly_version)))
    sv_type = args.sv_type
    pe_dump = False

    if delly_version[1] == 7:
        if delly_version[2] > 6:
            pe_dump = True
            if sv_type == 'TRA':
                sv_type = 'BND'
                log.log.info('Rename sv_type %s to %s' % (args.sv_type,
                                                          sv_type))
    elif delly_version[1] > 7:
        pe_dump = True
        if sv_type == 'TRA':
            sv_type = 'BND'
            log.log.info('Rename sv_type %s to %s' % (args.sv_type, sv_type))

    if pe_dump is True:
        output_tmp = '%s_tmp' % output
    else:
        output_tmp = output
    log.log.info('Preparing delly command line')
    delly_cmd = ['delly', 'call', '-t', sv_type, '-g', genome,
                 '-s', '15', '-q', '20', '-r', '30', '-o', '%s.bcf' % output_tmp]
    if args.no_exclude is False:
        log.log.info('Add exclude regions argument')
        delly_cmd += ['-x', ref_exclude]
    if pe_dump is True:
        log.log.info('Add pe_dump argument')
        delly_cmd += ['-p', '%s_pe_dump.txt.gz' % output]
    delly_cmd += [args.tumor, args.normal]
    log.log.info('%s' % ' '.join(delly_cmd))
    vcf_rn_cmd = ['sv_add_rn.py', '-v', '%s.bcf' % output_tmp,
                  '-p', '%s_pe_dump.txt.gz' % output,
                  '-o', '%s.bcf' % output]
    bcf_idx_cmd = ['bcftools', 'index', '%s.bcf' % output]

    delly_somatic_filter = ['delly', 'filter', '-t', sv_type,
                            '-f', 'somatic',
                            '-o', '%s.somatic.bcf' % output,
                            '-s', '%s.samples.tsv' % output,
                            '%s.bcf' % output]
    tumor_sample = get_sample_name(profile, args.tumor)
    normal_sample = get_sample_name(profile, args.normal)

    with open('%s.samples.tsv' % output, 'wt') as samples_tab:
        samples_tab.write('%s\tcontrol\n' % normal_sample)
        samples_tab.write('%s\ttumor\n' % tumor_sample)

    delly_germline_filter = ['delly', 'filter', '-t', sv_type,
                             '-f', 'germline',
                             '-o', '%s.germline.bcf' % output,
                             '%s.bcf' % output]
    delly_cmd = shlex.split(' '.join(map(str, delly_cmd)))
    vcf_rn_cmd = shlex.split(' '.join(map(str, vcf_rn_cmd)))
    bcf_idx_cmd = shlex.split(' '.join(map(str, bcf_idx_cmd)))
    somatic_cmd = shlex.split(' '.join(map(str, delly_somatic_filter)))
    germline_cmd = shlex.split(' '.join(map(str, delly_germline_filter)))
    
    log.log.info(' '.join(map(str, delly_cmd)))
    log.log.info('Execute delly with python subprocess.Popen')
    delly_proc = subprocess.Popen(delly_cmd)
    out0 = delly_proc.communicate()[0]
    if pe_dump is True:
        log.log.info('Add readname information to BCF/VCF')
        log.log.info(' '.join(map(str, vcf_rn_cmd)))
        vcf_rn_proc = subprocess.Popen(vcf_rn_cmd)
        out_rn = vcf_rn_proc.communicate()[0]
        log.log.info('Index the BCF/VCF containing the readname')
        log.log.info(' '.join(map(str, bcf_idx_cmd)))
        bcf_idx_proc = subprocess.Popen(bcf_idx_cmd)
        out_idx = bcf_idx_proc.communicate()[0]
    log.log.info('Apply delly somatic filter')
    log.log.info(' '.join(map(str, somatic_cmd)))
    somatic_proc = subprocess.Popen(somatic_cmd)
    out1 = somatic_proc.communicate()[0]

    log.log.info('Apply delly germline filter')
    log.log.info(' '.join(map(str, germline_cmd)))
    germline_proc = subprocess.Popen(germline_cmd)
    out2 = germline_proc.communicate()[0]
    log.log.info('Terminate delly')


def get_sample_name(profile, bam):
    module = get_module_cmd()
    module('add', 'tools')
    module('add', program_string(profile.programs['samtools_1']))
    get_headers = ['samtools', 'view', '-H']
    get_headers.append(bam)
    get_headers_cmd = shlex.split(' '.join(map(str, get_headers)))
    get_header_proc = subprocess.Popen(get_headers_cmd, stdout=subprocess.PIPE,
                                       bufsize=4096)
    for line in get_header_proc.stdout:
        if line.startswith('@RG'):
            line = line.strip().split('\t')
            for item in line:
                if item.startswith('SM:'):
                    item = re.sub('^SM:', '', item)
                    return(item)
def program_version(program):
    '''
    Parse the delly help message in attempt to
    return the software version in a list
    [major, minor, *]
    '''
    proc1 = subprocess.Popen([program], stdout=subprocess.PIPE)
    for line in proc1.stdout:
        if b'Version:' in line:
            version_text = line[line.find('(') + 1:line.find(')')]
            return map(int, version_text.rstrip().split(b' ')[1].split(b'.'))

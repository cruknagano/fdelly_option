FROM docker.io/weischenfeldt/computerome_weischenfeldt:1.0.5
MAINTAINER Francesco Favero <francesco.favero@bric.ku.dk>

USER root

ADD pype_modules/repos.yaml /root/repos.yaml

RUN pip install --no-cache-dir bio_pype==0.9.9 \
    && pype repos --repo /root/repos.yaml install weischenfeldt_stable \
    && pype repos --repo /root/repos.yaml install --force weischenfeldt_docker \
    && rm /root/repos.yaml \
    && mkdir /databases && chmod -R 7777 /databases \
    && mkdir /data && chmod -R 7777 /data

VOLUME /databases /data
ADD docs/* /opt/finsen_docs/
ADD scripts/run_delly_pipeline.py /usr/bin/run_delly_pipeline
ADD scripts/proc_time.py /usr/bin/proc_time.py
RUN chmod +x /usr/bin/run_delly_pipeline
RUN chmod +x /usr/bin/proc_time.py

RUN rm -f /usr/lib/python2.7/site-packages/pype/snippets/delly.py
COPY delly.py /usr/lib/python2.7/site-packages/pype/snippets/

RUN rm -f /usr/lib/python2.7/site-packages/pype/queues/parallel.py
COPY parallel.py /usr/lib/python2.7/site-packages/pype/queues/

RUN rm -rf /services/weischenfeldt_lab/software/sequenza-utils/2.2.0/bin/python
RUN rm -rf /services/weischenfeldt_lab/software/sequenza-utils/2.2.0/bin/python2.7
RUN rm -rf /services/weischenfeldt_lab/software/sequenza-utils/2.2.0/bin/python2
RUN mkdir -p /home/projects/cu_10027/apps
RUN cp -r /services/weischenfeldt_lab/software /home/projects/cu_10027/apps/

RUN pip install --upgrade pip
RUN pip install pybedtools --upgrade

USER docker

WORKDIR /home/docker

CMD ["/bin/bash"]

# Delly workflow using bio_pype

This docker images is used to run delly pipeline as it is implemented in the computerome HPC system


### DELLY

[DELLY](https://github.com/tobiasrausch/delly) is an integrated structural variant prediction method that can detect deletions, tandem duplications, inversions, insertions and translocations at single-nucleotide resolution in short-read massively parallel sequencing data. It uses paired-ends and split-reads to sensitively and accurately delineate genomic rearrangements throughout the genome.


## Panel of normal

The collection of SVs from 1000 genomes project and PCAWG is available from synapse

```
synapse get syn9934121
```

The tar.gz archive needs to be provided as local file in the Dockstore `json`.
After downloading the file, for example in the `/tmp` folder, edit the `sv-collection` entry ion the json as:

```
"sv-collection": {
    "path": "/tmp/svCallCollectionsPCAWG.tar.gz",
    "class": "File"
},
```

or provide the path of the file in the run script arguments if run interactively from a Docker container

## GC content wiggle file

The `gc_wig` parameter in the cwl consists in a GC content wiggle file. The parameter is optional, if it is not provided the workflow would generate a `wig` file using the provided `fasta` reference file.
A gc wiggle of the human genome hs37d5, is available at [bricweb](https://bricweb.sund.ku.dk/bric-data/francesco/genome.gc.wig.gz)

Once the file has been dowloaded, it can be added in the `Dockstore.json` file as following:

```
  "gc_wig": {
    "path": "/tmp/genome.gc.wig.gz",
    "class": "File"
  },
```

## Building

You need Docker installed in order to perform this build.

    cd finsen_delly_workflow
    docker build -t finsen_delly_workflow:latest .


*Local test:*

    $> dockstore tool launch --entry Dockstore.cwl --local-entry --json sample_configs.local.json

